﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankSpawn : Message
    {
        public string Username;

        public float xPos;
        public float yPos;
        public float zPos;

        public float xDest;
        public float yDest;
        public float zDest;

        public float xAim;
        public float yAim;
        public float zAim;

        public float orientation;

        public MessageTankSpawn(string username) : this(username, 0f, 0f, 0f) { }
        public MessageTankSpawn(string username, float x, float y, float z) : this(username, x, y, z, x, y, z, 0f, 0f, 0f, 0f){ }
        public MessageTankSpawn(string username, float xpos, float ypos, float zpos, float xdest, float ydest, float zdest, float xaim, float yaim, float zaim, float orientation)
        {
            this.Username = username;
            this.xPos = xpos;
            this.yPos = ypos;
            this.zPos = zpos;
            this.xDest = xdest;
            this.yDest = ydest;
            this.zDest = ydest;
            this.xAim = xaim;
            this.yAim = yaim;
            this.zAim = zaim;
            this.orientation = orientation;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.Username = reader.ReadString();
            this.xPos = reader.ReadSingle();
            this.yPos = reader.ReadSingle();
            this.zPos = reader.ReadSingle();
            this.xDest = reader.ReadSingle();
            this.yDest = reader.ReadSingle();
            this.zDest = reader.ReadSingle();
            this.xAim = reader.ReadSingle();
            this.yAim = reader.ReadSingle();
            this.zAim = reader.ReadSingle();
            this.orientation = reader.ReadSingle();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(this.Username);
            writer.Write(this.xPos);
            writer.Write(this.yPos);
            writer.Write(this.zPos);
            writer.Write(this.xDest);
            writer.Write(this.yDest);
            writer.Write(this.zDest);
            writer.Write(this.xAim);
            writer.Write(this.yAim);
            writer.Write(this.zAim);
            writer.Write(this.orientation);
        }
    }
}
