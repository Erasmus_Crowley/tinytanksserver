﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankListRequest : Message
    {
        public override void DeserializeFrom(BinaryReader reader)
        {
            //No data required. Do nothing.
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            //No data required. Do nothing.
        }
    }
}
