﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageUsernameResponse : Message
    {
        public bool Accepted = false;

        public MessageUsernameResponse(bool accepted)
        {
            this.Accepted = accepted;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            Accepted = reader.ReadBoolean();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(Accepted);
        }
    }
}
