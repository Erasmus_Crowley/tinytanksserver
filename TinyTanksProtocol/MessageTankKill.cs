﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankKill : Message
    {
        public string Username;

        public MessageTankKill(string username)
        {
            this.Username = username;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.Username = reader.ReadString();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(this.Username);
        }
    }
}
