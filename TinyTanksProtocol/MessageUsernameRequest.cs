﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageUsernameRequest : Message
    {
        public string Username;

        public MessageUsernameRequest(string username)
        {
            this.Username = username;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            Username = reader.ReadString();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(Username);
        }
    }
}
