﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    /// <summary>
    /// Message used to report tank damage to the server.
    /// Proximity = Nearness to the epicenter of the explosion. 0 = no damage. 1+ = full damage.
    /// </summary>
    public class MessageTankDamageRequest : Message
    {
        public float Proximity;

        public MessageTankDamageRequest(float proximity)
        {
            this.Proximity = proximity;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.Proximity = reader.ReadSingle();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(this.Proximity);
        }
    }
}
