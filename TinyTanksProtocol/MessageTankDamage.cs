﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankDamage : Message
    {
        public string Username;
        public float Health;

        public MessageTankDamage(string username, float health)
        {
            this.Username = username;
            this.Health = health;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.Username = reader.ReadString();
            this.Health = reader.ReadSingle();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(this.Username);
            writer.Write(this.Health);
        }
    }
}
