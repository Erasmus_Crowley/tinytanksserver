﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankPositionUpdate : Message
    {
        public float X;
        public float Y;
        public float Z;
        public float Orientation;

        public MessageTankPositionUpdate(float x, float y, float z, float orientation)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.Orientation = orientation;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.X = reader.ReadSingle();
            this.Y = reader.ReadSingle();
            this.Z = reader.ReadSingle();
            this.Orientation = reader.ReadSingle();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Z);
            writer.Write(Orientation);
        }
    }
}
