﻿using Metatron;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyTanksProtocol
{
    public class MessageTankFire : Message
    {
        public string Username;
        public float X;
        public float Y;
        public float Z;

        public MessageTankFire(string username, float x, float y, float z)
        {
            this.Username = username;
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public override void DeserializeFrom(BinaryReader reader)
        {
            this.Username = reader.ReadString();
            this.X = reader.ReadSingle();
            this.Y = reader.ReadSingle();
            this.Z = reader.ReadSingle();
        }

        public override void SerializeTo(BinaryWriter writer)
        {
            writer.Write(this.Username);
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Z);
        }
    }
}
