﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyTanksProtocol;

namespace TinyTanksServer
{
    public class Tank
    {
        private MessageTankSpawn _spawnMessage;

        public static float MaxHealth = 100;

        public string Username;
        public float Health;

        public float xPos;
        public float yPos;
        public float zPos;

        public float xDest;
        public float yDest;
        public float zDest;

        public float xAim;
        public float yAim;
        public float zAim;

        public float orientation;

        public Tank(string username)
        {
            this.Username = username;
            this.Health = Tank.MaxHealth;

            this._spawnMessage = new MessageTankSpawn(username);
        }

        public MessageTankSpawn SpawnMessage
        {
            get
            {
                this._spawnMessage.xPos = this.xPos;
                this._spawnMessage.yPos = this.yPos;
                this._spawnMessage.zPos = this.zPos;

                this._spawnMessage.xDest = this.xDest;
                this._spawnMessage.yDest = this.yDest;
                this._spawnMessage.zDest = this.zDest;

                this._spawnMessage.xAim = this.xAim;
                this._spawnMessage.yAim = this.yAim;
                this._spawnMessage.zAim = this.zAim;

                this._spawnMessage.orientation = this.orientation;

                return this._spawnMessage;
            }
        }
    }
}
