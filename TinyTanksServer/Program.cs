﻿using Metatron;
using Metatron.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyTanksProtocol;

namespace TinyTanksServer
{
    class Program
    {
        private static float ExplosionDamage = 50f; //damage max, out of 100f;
        private static float ExplosionRange = 2f; //range in meters. For ref, tanks are 2 meters long.

        private static NetServer server;
        private static Dictionary<Connection, Tank> users = new Dictionary<Connection, Tank>();
        private static HashSet<string> usernames = new HashSet<string>();

        static void Main(string[] args)
        {
            Console.WriteLine("TinyTanks server!");

            server = new NetServer(20000);
            server.OnClientConnected += OnClientConnected;
            server.OnClientDisconnected += OnClientDisconnected;

            server.AddMessageHandler(typeof(MessageUsernameRequest), OnClientRequestsUsername);
            server.AddMessageHandler(typeof(MessageTankListRequest), OnClientRequestTankList);
            server.AddMessageHandler(typeof(MessageTankSpawnRequest), OnClientRequestSpawn);
            server.AddMessageHandler(typeof(MessageTankMoveRequest), OnClientRequestMove);
            server.AddMessageHandler(typeof(MessageTankAimRequest), OnClientRequestAim);
            server.AddMessageHandler(typeof(MessageTankFireRequest), OnClientRequestFire);
            server.AddMessageHandler(typeof(MessageTankDamageRequest), OnClientReportDamage);
            server.AddMessageHandler(typeof(MessageTankPositionUpdate), OnClientReportPosition);

            string input;

            //Start listening when the server starts!
            Start();

            while (true)
            {
                input = Console.ReadLine();

                if (input.Equals("quit", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (server != null && !server.Running)
                        Stop();

                    break;
                }

                if (input.Equals("start", StringComparison.InvariantCultureIgnoreCase))
                {
                    Start();
                }

                if (input.Equals("stop", StringComparison.InvariantCultureIgnoreCase))
                {
                    Stop();
                }
            }

            server.OnClientConnected -= OnClientConnected;
            server.OnClientDisconnected -= OnClientDisconnected;
        }


        public static void Start()
        {
            if (!server.Running)
            {
                server.Start();

                Console.WriteLine("Server started. Listening on port " + server.Port + ".");
            }
        }

        public static void Stop()
        {
            if (server.Running)
            {
                server.Stop();

                Console.WriteLine("Server stopped.");
            }
        }

        private static void BroadcastMessage(Message message)
        {
            foreach (Connection c in users.Keys)
            {
                c.Send(message);
            }
        }

        #region Client Events

        private static void OnClientConnected(Connection connection)
        {
            //Console.WriteLine("Client connected!");
        }

        private static void OnClientDisconnected(Connection connection)
        {
            //Console.WriteLine("Client disconnected!");
            if (users.ContainsKey(connection))
            {
                if (users.ContainsKey(connection))
                {
                    usernames.Remove(users[connection].Username);

                }

                MessageTankKill kill = new MessageTankKill(users[connection].Username);

                users.Remove(connection);
                BroadcastMessage(kill);
            }
        }

        #endregion

        #region Message Handlers

        private static void OnClientRequestsUsername(Connection sender, Message message)
        {
            MessageUsernameRequest request = (MessageUsernameRequest)message;

            if (request != null)
            {
                if (usernames.Contains(request.Username))
                {
                    //Username denied!
                    sender.Send(new MessageUsernameResponse(false));
                    return;
                }

                if (users.ContainsKey(sender))
                {
                    //Change name!
                    usernames.Remove(users[sender].Username);
                    usernames.Add(request.Username);

                    users[sender].Username = request.Username;
                }

                //New user!
                usernames.Add(request.Username);
                users.Add(sender, new Tank(request.Username));

                sender.Send(new MessageUsernameResponse(true));
            }
        }

        private static void OnClientRequestSpawn(Connection sender, Message message)
        {
            if (users.ContainsKey(sender))
            {
                MessageTankSpawnRequest request = (MessageTankSpawnRequest)message;

                if (request != null)
                {
                    Tank t = users[sender];

                    t.Health = Tank.MaxHealth;

                    t.xPos = request.X;
                    t.yPos = request.Y;
                    t.zPos = request.Z;

                    t.xAim = request.X - 1f;
                    t.xAim = request.Y;
                    t.zAim = request.Z;

                    t.xDest = request.X;
                    t.yDest = request.Y;
                    t.zDest = request.Z;

                    t.orientation = 0f;

                    BroadcastMessage(t.SpawnMessage);
                }
            }
        }

        private static void OnClientRequestTankList(Connection sender, Message message)
        {
            MessageTankListRequest request = (MessageTankListRequest)message;

            if (request != null)
            {
                foreach (Tank t in users.Values)
                {
                    sender.Send(t.SpawnMessage);
                }
            }
        }

        private static void OnClientRequestMove(Connection sender, Message message)
        {
            if (users.ContainsKey(sender) && users.ContainsKey(sender))
            {
                MessageTankMoveRequest request = (MessageTankMoveRequest)message;

                if (request != null)
                {
                    Tank t = users[sender];

                    t.xDest = request.X;
                    t.yDest = request.Y;
                    t.zDest = request.Z;

                    BroadcastMessage(new MessageTankMove(t.Username, request.X, request.Y, request.Z));
                }
            }
        }

        private static void OnClientRequestAim(Connection sender, Message message)
        {
            if (users.ContainsKey(sender) && users.ContainsKey(sender))
            {
                MessageTankAimRequest request = (MessageTankAimRequest)message;

                if (request != null)
                {
                    Tank t = users[sender];
                    t.xAim = request.X;
                    t.yAim = request.Y;
                    t.zAim = request.Z;

                    BroadcastMessage(new MessageTankAim(t.Username, t.xAim, t.yAim, t.zAim));
                }
            }
        }

        private static void OnClientReportPosition(Connection sender, Message message)
        {
            //Update the position to stored in memory.
            // this is used to tell future clients where to spawn the tank.
            if (users.ContainsKey(sender) && users.ContainsKey(sender))
            {
                MessageTankPositionUpdate request = (MessageTankPositionUpdate)message;

                if (request != null)
                {
                    Tank t = users[sender];
                    t.xPos = request.X;
                    t.yPos = request.Y;
                    t.zPos = request.Z;
                    t.orientation = request.Orientation;
                }
            }
        }

        private static void OnClientRequestFire(Connection sender, Message message)
        {
            if (users.ContainsKey(sender) && users[sender].Health > 0f)
            {
                MessageTankFireRequest request = (MessageTankFireRequest)message;

                if (request != null)
                {
                    BroadcastMessage(new MessageTankFire(users[sender].Username, request.X, request.Y, request.Z));
                }
            }
        }

        private static void OnClientReportDamage(Connection sender, Message message)
        {
            if (users.ContainsKey(sender))
            {
                MessageTankDamageRequest request = (MessageTankDamageRequest)message;

                if (request != null)
                {
                    if (request.Proximity > ExplosionRange)
                        return;

                    float power = request.Proximity / ExplosionRange;
                    power = Math.Min(power, 1f);
                    power = Math.Max(power, 0f);

                    users[sender].Health -= power * ExplosionDamage;

                    if (users[sender].Health <= 0)
                    {
                        BroadcastMessage(new MessageTankKill(users[sender].Username));
                    }
                    else
                    {
                        BroadcastMessage(new MessageTankDamage(users[sender].Username, users[sender].Health / Tank.MaxHealth));
                    }
                }
            }
        }

        #endregion


    }
}
